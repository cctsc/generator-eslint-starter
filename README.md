# generator-eslint-starter [Deprecated]

> generator-eslint-starter has been deprecated.

> A [Yeoman](http://yeoman.io/) generator for installing and configuring [ESLint](http://eslint.org/).

This generator allows you to install ESLint and some of the most popular shareable configurations. Also allows you to create your own configuration for ESLint rules.

## Installation

Make sure you have [Node](https://nodejs.org/en/) and [npm](https://www.npmjs.com/) installed.
Then install [Yeoman](http://yeoman.io/) and generator:
```bash
npm install -g yo
npm install -g generator-eslint-starter
```

Create a new directory for your project:
```bash
mkdir my-new-project
cd my-new-project
``` 

Now you can install and configure ESLint with:
```bash
yo eslint-starter
```

## Supported shareable configs
* [Google](https://www.npmjs.com/package/eslint-config-google)
* [Airbnb](https://www.npmjs.com/package/eslint-config-airbnb)
* [Standard](https://www.npmjs.com/package/eslint-config-standard)

## Tests
You can run simple tests with:
```bash
npm test
```

Additionally you can check code with ESLint:
```bash
npm run eslint
```

## License
MIT © [Paweł Halczuk](https://bitbucket.org/cctsc/)
