var Generator = require('yeoman-generator');
var prompts = require('./util/prompts.js');
var getSettings = require('./util/getSettings.js');
var chalk = require('chalk');
var yosay = require('yosay');

module.exports = class extends Generator {
  initializing() {
    this.dependencies = ['eslint'];
  }

  prompting() {
    this.log(yosay(
      `Welcome to the ${chalk.green('eslint-starter')}!`
    ));

    return this.prompt(prompts)
               .then(answers => {
                 this.settings = getSettings(answers);
                 this.filesToLint = answers.jsPath;
               });
  }

  determineDependencies() {
    if (this.settings.extends.includes('google')) {
      this.dependencies.push('eslint-config-google');
    }

    if (this.settings.extends.includes('airbnb')) {
      this.dependencies.push('eslint-config-airbnb', 'eslint-plugin-import',
                             'eslint-plugin-react', 'eslint-plugin-jsx-a11y');
    }

    if (this.settings.extends.includes('standard')) {
      this.dependencies.push('eslint-config-standard', 'eslint-plugin-standard',
                             'eslint-plugin-promise', 'eslint-plugin-import',
                             'eslint-plugin-node');
    }
  }

  writing() {
    this.fs.write(
      this.destinationPath('.eslintrc.js'),
      `module.exports = ${JSON.stringify(this.settings, null, 2)};`
    );

    this.fs.extendJSON(
      this.destinationPath('package.json'),
      {
        scripts: {
          eslint: `eslint --fix --cache -c .eslintrc.js "${this.filesToLint}"`
        }
      }
    );

    this.log(chalk.green(`Adding script to a ${chalk.white('package.json')} file.`));
    this.log(chalk.green(`Run ${chalk.bold('eslint')} to lint your JS files.`));
  }

  install() {
    this.yarnInstall(this.dependencies, {dev: true});
    this.installDependencies({
      yarn: true,
      npm: true,
      bower: false
    });
  }

  end() {
    this.log(chalk.green('... We are done!'));
    this.log(chalk.green(`Thanks for using ${chalk.bold('eslint-starter')}. Hava a nice day!`));
  }
}
