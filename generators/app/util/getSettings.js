'use strict';

module.exports = function getSettings(answers) {
  var rules = {},
      env = {};

  function createRule(fromAnswer, asError) {
    fromAnswer.forEach((elem) => {
      rules[elem] = [asError ? "error" : "warn"];
    });
  }

  if (answers.environments.length > 0) {
    answers.environments.forEach((elem) => {
      env[elem] = true;
    });
  }

  if (answers.possible) {
    createRule(answers.possibleWarn, false);
    createRule(answers.possibleError, true);
  }

  if (answers.best) {
    createRule(answers.bestWarn, false);
    createRule(answers.bestError, true);
  }

  if (answers.variable) {
    createRule(answers.variableWarn, false);
    createRule(answers.variableError, true);
  }

  if (answers.node) {
    createRule(answers.nodeWarn, false);
    createRule(answers.nodeError, true);
  }

  if (answers.styl) {
    createRule(answers.stylWarn, false);
    createRule(answers.stylError, true);
  }

  if (answers.es6rules) {
    createRule(answers.es6rulesWarn, false);
    createRule(answers.es6rulesError, true);
  }

  return {
    extends: answers.extends,
    parserOptions: {
      ecmaVersion: parseInt(answers.ecmaVersion, 10),
      sourceType: answers.sourceType ? "module" : "script",
      ecmaFeatures: {
        impliedStrict: answers.impliedStrict,
        jsx: answers.jsx
      }
    },
    env: env,
    rules: rules
  };
};
