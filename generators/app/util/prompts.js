'use strict';

var rules = require('./rules.js');

module.exports = [
  {
    type: 'input',
    name: 'jsPath',
    message: 'What is the location of your JS files?',
    default: 'src/js'
  },
  {
    type: 'input',
    name: 'ecmaVersion',
    message: 'Which version of ECMAScript syntax you want to use? For example: 5, 6 or 2015',
    default: 5,
  },
  {
    type: 'checkbox',
    name: 'extends',
    message: 'Do you want to use any of those presets?',
    choices: [{
      name: 'Default ESLint',
      value: 'eslint:recommended',
      checked: true
    },
    {
      name: "AirBnB",
      value: 'airbnb'
    },
    {
      name: "Google",
      value: 'google'
    },
    {
      name: "Standard",
      value: 'standard'
    }]
  },
  {
    type: 'confirm',
    name: 'sourceType',
    message: 'Do you use ECMAScript modules?',
    default: false
  },
  {
    type: 'confirm',
    name: 'impliedStrict',
    message: 'Do you want to enable global strict mode (for ES5+)?',
    default: false
  },
  {
    type: 'confirm',
    name: 'jsx',
    message: 'Do you use JSX syntax?',
    default: false
  },
  {
    type: 'checkbox',
    name: 'environments',
    message: 'Which environments you want to enable:',
    choices: ['browser', 'node', 'es6', 'commonjs', 'shared-node-browser',
              'worker', 'amd', 'mocha', 'jasmine', 'jest', 'phantomjs',
              'protractor', 'qunit', 'jquery', 'prototypejs', 'shelljs',
              'meteor', 'mongo', 'applescript', 'nashorn', 'serviceworker',
              'atomtest', 'embertest', 'webextensions', 'greasemonkey'],
    default: ['browser']
  },
  {
    type: 'confirm',
    name: 'possible',
    message: 'Do you want to configure rules related to syntax and logical errors?',
    default: false
  },
  {
    type: 'checkbox',
    name: 'possibleWarn',
    message: 'Select the rules that should generate a warning:',
    choices: rules.possible,
    when: function(answers) { return answers.possible; }
  },
  {
    type: 'checkbox',
    name: 'possibleError',
    message: 'Select the rules that should generate an error:',
    choices: function(answers) {
      var warnings = answers.possibleWarn;

      return rules.possible.filter((item) => {return !(warnings.includes(item));});
    },
    when: function(answers) { return answers.possible; }
  },
  {
    type: 'confirm',
    name: 'best',
    message: 'Do you want to configure rules related to best practices?',
    default: false
  },
  {
    type: 'checkbox',
    name: 'bestWarn',
    message: 'Select the rules that should generate a warning:',
    choices: rules.best,
    when: function(answers) { return answers.best; }
  },
  {
    type: 'checkbox',
    name: 'bestError',
    message: 'Select the rules that should generate an error:',
    choices: function(answers) {
      var warnings = answers.bestWarn;

      return rules.best.filter((item) => {return !(warnings.includes(item));});
    },
    when: function(answers) { return answers.best; }
  },
  {
    type: 'confirm',
    name: 'variable',
    message: 'Do you want to configure rules related to variable declarations?',
    default: false
  },
  {
    type: 'checkbox',
    name: 'variableWarn',
    message: 'Select the rules that should generate a warning:',
    choices: rules.variable,
    when: function(answers) { return answers.variable; }
  },
  {
    type: 'checkbox',
    name: 'variableError',
    message: 'Select the rules that should generate an error:',
    choices: function(answers) {
      var warnings = answers.variableWarn;

      return rules.variable.filter((item) => {return !(warnings.includes(item));});
    },
    when: function(answers) { return answers.variable; }
  },
  {
    type: 'confirm',
    name: 'node',
    message: 'Do you want to configure rules related to Node and CommonJS?',
    default: false
  },
  {
    type: 'checkbox',
    name: 'nodeWarn',
    message: 'Select the rules that should generate a warning:',
    choices: rules.node,
    when: function(answers) { return answers.node; }
  },
  {
    type: 'checkbox',
    name: 'nodeError',
    message: 'Select the rules that should generate an error:',
    choices: function(answers) {
      var warnings = answers.nodeWarn;

      return rules.node.filter((item) => {return !(warnings.includes(item));});
    },
    when: function(answers) { return answers.node; }
  },
  {
    type: 'confirm',
    name: 'styl',
    message: 'Do you want to configure rules related to stylistic issues?',
    default: false
  },
  {
    type: 'checkbox',
    name: 'stylWarn',
    message: 'Select the rules that should generate a warning:',
    choices: rules.styl,
    when: function(answers) { return answers.styl; }
  },
  {
    type: 'checkbox',
    name: 'stylError',
    message: 'Select the rules that should generate an error:',
    choices: function(answers) {
      var warnings = answers.stylWarn;

      return rules.styl.filter((item) => {return !(warnings.includes(item));});
    },
    when: function(answers) { return answers.styl; }
  },
  {
    type: 'confirm',
    name: 'es6rules',
    message: 'Do you want to configure rules related to ECMAScript 6?',
    default: false
  },
  {
    type: 'checkbox',
    name: 'es6rulesWarn',
    message: 'Select the rules that should generate a warning:',
    choices: rules.es6rules,
    when: function(answers) { return answers.es6rules; }
  },
  {
    type: 'checkbox',
    name: 'es6rulesError',
    message: 'Select the rules that should generate an error:',
    choices: function(answers) {
      var warnings = answers.es6rulesWarn;

      return rules.es6rules.filter((item) => {return !(warnings.includes(item));});
    },
    when: function(answers) { return answers.es6rules; }
  },
];
