var helpers = require('yeoman-test');
var assert = require('yeoman-assert');
var path = require('path');

var getSettings = require('../generators/app/util/getSettings.js');

var promptAnswers = {
  jsPath: 'src/js',
  ecmaVersion: 5,
  extends: ['eslint:recommended'],
  sourceType: false,
  impliedStrict: false,
  jsx: false,
  possible: true,
  best: true,
  variable: false,
  node: false,
  styl: false,
  es6rules: false,
  environments: ['browser', 'node'],
  possibleWarn: ['for-direction', 'no-await-in-loop'],
  possibleError: ['no-cond-assign', 'no-console'],
  bestWarn: ['accessor-pairs', 'array-callback-return'],
  bestError: ['block-scoped-var', 'complexity']
};

describe('getSettings util', function() {
  it('can be imported', function() {
    assert.ok(getSettings);
  });

  it('create correct env and rules', function() {
    var settings = getSettings(promptAnswers);

    assert.implement(
      settings,
      {
        env: {
          browser: true,
          node: true
        },
        rules: {
          'for-direction': ["warn"],
          'no-await-in-loop': ["warn"],
          'no-cond-assign': ["error"],
          'no-console': ["error"],
          'accessor-pairs': ["warn"],
          'array-callback-return': ["warn"],
          'block-scoped-var': ["error"],
          'complexity': ["error"]
        }
      });
  });

  it('returns settings object', function() {
    var settings = getSettings(promptAnswers);

    assert.deepEqual(
      settings,
      {
        extends: ['eslint:recommended'],
        parserOptions: {
          ecmaVersion: 5,
          sourceType: "script",
          ecmaFeatures: {
            impliedStrict: false,
            jsx: false
          }
        },
        env: {
          browser: true,
          node: true
        },
        rules: {
          'for-direction': ["warn"],
          'no-await-in-loop': ["warn"],
          'no-cond-assign': ["error"],
          'no-console': ["error"],
          'accessor-pairs': ["warn"],
          'array-callback-return': ["warn"],
          'block-scoped-var': ["error"],
          'complexity': ["error"]
        }
      }
    );
  });
});

describe('eslint-starter', function() {
  before('create RunContext', function() {
    this.timeout(0);

    return helpers.run(path.join(__dirname, '../generators/app'))
                  .withPrompts(promptAnswers);
  });

  it('the generator can be required without throwing', function() {
    require('../generators/app/index.js');
  });

  it('generate .eslintre.js', function() {
    assert.file('.eslintrc.js');
  });

  it('add scripts to package.json', function() {
    assert.jsonFileContent(
      'package.json',
      {
        scripts: {
          eslint: 'eslint --fix --cache -c .eslintrc.js "src/js"'
        }
      }
    );
  });
});
